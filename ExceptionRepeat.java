
import java.sql.SQLIntegrityConstraintViolationException;

public class ExceptionRepeat extends SQLIntegrityConstraintViolationException {
    public ExceptionRepeat(String err){
        super(err);
    }
}
