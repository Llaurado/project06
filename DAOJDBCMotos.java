import org.apache.log4j.Logger;

import java.net.ConnectException;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOJDBCMotos extends DAOMotos {
    private List<Motos> motos;
//    final static Logger logger = Logger.getLogger(DAOJDBCMotos.class.getName());

    private static Connection myconn;


    @Override
    public void getConnection(String url, String user, String pass) throws ExceptionConnect {
        ConexioJDBC conexioJDBC = new ConexioJDBC();
        myconn = conexioJDBC.connec(url, user, pass);

    }

    @Override
    public void crearTaula() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "motos", null);
            Statement statement = myconn.createStatement();


            if (!tables.next()) {
                String query = " create table motos(nom varchar(15),matricula varchar(7) primary key,data_matriculacio date);";
                statement.execute(query);
                System.out.println("Taula creada");
            }
            System.out.println("La taula ja existeix pots comencçar a treballar amb ella");
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }

    }

    @Override
    public void borrarTaula() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "motos", null);
            Statement statement = myconn.createStatement();

            if (tables.next()) {
                String query = " drop table motos";
                statement.execute(query);
                System.out.println("Taula eliminada");
            }
            System.out.println("La taula no existeix");
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }

    }


    @Override
    public List<Motos> getAll() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            Statement mystmn = myconn.createStatement();
            String query = "select * from motos";
            ResultSet rs = mystmn.executeQuery(query);
            Motos motos1 = null;
            while (rs.next()) {

                motos1.setMatricula(rs.getString(2));
                motos1.setNom(rs.getString(1));
                motos1.setData_matriculacio(rs.getString(3));
                motos.add(motos1);
            }
            return motos;
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }


    @Override
    public void inserir(Motos motos) throws ExceptionConnect {
        try {

            String sentenciaSQL = "insert into motos (nom,matricula,data_matriculacio)  values (?,?,?)";

            PreparedStatement sentenciaPreparada = myconn.prepareStatement(sentenciaSQL);
            sentenciaPreparada.setString(1, motos.getNom());
            sentenciaPreparada.setString(2, motos.getMatricula());
            sentenciaPreparada.setString(3, motos.getData_matriculacio());
            sentenciaPreparada.executeUpdate();

            System.out.println("Client inserit correctament.");


        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void recuperarPerId(String id) throws ExceptionConnect {

        try {
            String sentenciaSQL = "select * from moto where matricula=" + id;

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getDate(3));
            }
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void recuperarTots() throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            String sentenciaSQL = "select * from motos";

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getDate(3));
            }
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void update(Motos motos, String[] params) throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }


            String query = "update motos set nom=" + params[0] + ",matricula" + params[1] + ",data_matriculacio=" + params[2] + " where matricula =" + motos.getMatricula();
            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(query);
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getInt(2) + " " + rs.getDate(3));
            }
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void delete(Motos motos) throws ExceptionConnect {
        try {
            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }


            String sentenciaSql = "delete from motos where matricula=" + motos.getMatricula();


            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSql);
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getInt(2) + " " + rs.getDate(3));
            }
            System.out.println("s'ha eliminat la moto :" + motos.toString());
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void closeConnection() {
        ConexioJDBC conexioJDBC = new ConexioJDBC();
        myconn = conexioJDBC.close();


    }
}
