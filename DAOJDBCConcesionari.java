import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;

public class DAOJDBCConcesionari extends DAOConcesionari {

    private static Connection myconn;
    private final List<Concesionari> concesionaris = new ArrayList<>();
//    final static Logger logger = Logger.getLogger(DAOJDBCConcesionari.class.getName());




    @Override
    public void getConnection(String url, String user, String pass)  {
                    ConexioJDBC conexioJDBC = new ConexioJDBC();
            myconn = conexioJDBC.connec(url, user, pass);


    }

    @Override
    public void crearTaula() throws ExceptionConnect {
        try {
            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "concesionari", null);
            Statement statement = myconn.createStatement();

            if (!tables.next()) {
                String query = " create table concesionari (dni_concesionari varchar(9) primary key,nom_concesionari varchar(15),cantmaxmotos int);";
                statement.execute(query);
                System.out.println("Taula creada");
            }
            System.out.println("La taula ja existeix pots comencçar a treballar amb ella");
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }


    }

    @Override
    public void borrarTaula() throws ExceptionConnect {
        try {
            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "concesionari", null);
            Statement statement = myconn.createStatement();

            if (tables.next()) {
                String query = " drop table concesionari";
                statement.execute(query);
                System.out.println("Taula eliminada");
            }
            System.out.println("La taula no existeix");
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }

    }

    @Override
    public List<Concesionari> getAll() throws ExceptionConnect {
        try {
            Statement mystmn = myconn.createStatement();
            Motos motos = null;
            Concesionari concesionari = null;
            String querymoto = "select * from motos";
            ResultSet resultSet1 = mystmn.executeQuery(querymoto);
            List<Motos> motos1 = new ArrayList<>();

            while (resultSet1.next()) {
                motos.setMatricula(resultSet1.getString(2));
                motos.setNom(resultSet1.getString(1));
                motos.setData_matriculacio(resultSet1.getString(3));
                motos1.add(motos);
            }

            String queryconcesionari = "select * from concesionari";
            ResultSet resultSet = mystmn.executeQuery(queryconcesionari);

            while (resultSet.next()) {
                System.out.println(resultSet.getString(1) + " " + resultSet.getString(2) + " " + resultSet.getInt(3));
                concesionari.setNom_concesionari(resultSet.getString(2));
                concesionari.setDni_concesionari(resultSet.getString(1));
                concesionari.setCantmaxmotos(resultSet.getInt(3));

                concesionaris.add(concesionari);

                for (int i = 0; i <= concesionaris.size(); i++) {
                    List<Motos> motoscantmax = new ArrayList<>();

                    if (i % 10 == 0) {
                        for (int j = i; j < i + 10; j++) {
                            motoscantmax.add(motos1.get(i));
                        }
                        concesionaris.get(i).setMagatzem_motos(motos1);

                    }

                }

            }

            return concesionaris;

        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void inserir(Concesionari clase) throws ExceptionConnect, ExceptionRepeat {
        try {

            String sentenciaSQL = "insert into concesionari (dni_concesionari,nom_concesionari,cantmaxmotos)  values (?,?,?)";
            PreparedStatement sentenciaPreparada = myconn.prepareStatement(sentenciaSQL);

            sentenciaPreparada.setString(1, clase.getDni_concesionari());
            sentenciaPreparada.setString(2, clase.getNom_concesionari());
            sentenciaPreparada.setInt(3, clase.getCantmaxmotos());
            sentenciaPreparada.executeUpdate();
            System.out.println("Client inserit correctament.");


            for (int i = 0; i < clase.getCantmaxmotos(); i++) {

                String queryarray = "insert into motos_conces(motos_matricula,dni_concesionari)values(?,?)";
                sentenciaPreparada = myconn.prepareStatement(queryarray);
                sentenciaPreparada.setString(1, clase.getMagatzem_motos().get(i).getMatricula());
                sentenciaPreparada.setString(2, clase.getDni_concesionari());

            }
        } catch (ExceptionRepeat e){
//            logger.error("error valor repetit ", e);
//            logger.getLevel();
            throw new ExceptionRepeat("error valor repetit");

        } catch (SQLException throwables) {

//                logger.error("No s'ha conectat correctament ", throwables);
//                logger.getLevel();
                throw new ExceptionConnect("error al conectar");

        }

    }

    @Override
    public void recuperarPerId(String id) throws ExceptionConnect {

        try {

            String sentenciaSQL = "select * from concesionari where dni_concesionari=" + id;
            String queryarray = "select * from motos_conces where dni_concesionari=" + id;
            Statement mystmn = myconn.createStatement();
            ResultSet rs = mystmn.executeQuery(sentenciaSQL);

            while (rs.next()) {
                System.out.println("Concesionari cercat");
                System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getInt(3));
            }

            rs = mystmn.executeQuery(queryarray);
            System.out.println("Matricules de les motos que pertanyen al concesionari amb dni" + id);

            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }


    }


    @Override
    public void recuperarTots() throws ExceptionConnect {
        String sentenciaSQL = "select * from concesionari";
        String queryarray = "select * from motos_conces";

        try {

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);
            System.out.println("Concesionaris");
            while (rs.next()) {
                System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getInt(3));
            }
            rs = mystmn.executeQuery(queryarray);
            System.out.println("motos per concesionari");
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }
        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();

            throw new ExceptionConnect("error al conectar");

        }

    }

    @Override
    public void update(Concesionari clase, String[] params) throws ExceptionConnect, ExceptionRepeat {
        try {
            String query = "update concesionari set dni_concesionari=" + params[0] + ",nom_concesionari" + params[1] + ",cantmaxmotos=" + params[2] + " where matricula =" + clase.getDni_concesionari();
            Statement mystmn = myconn.createStatement();
            ResultSet rs = mystmn.executeQuery(query);

            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getInt(3));
            }

        } catch (ExceptionRepeat e){
//            logger.error("error valor repetit ", e);
//            logger.getLevel();
            throw new ExceptionRepeat("error valor repetit");

        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void delete(Concesionari clase) throws ExceptionConnect {

        try {
            String sentenciaSql = "delete from concesionari where dni_concesionari=" + clase.getDni_concesionari();
            Statement mystmn = myconn.createStatement();
            ResultSet rs = mystmn.executeQuery(sentenciaSql);

            while (rs.next()) {
                System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getArray(3) + " " + rs.getInt(4));
            }

            System.out.println("s'ha eliminat el concesionari :" + clase.toString());

        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void moureMoto(Concesionari concesionari, String matricula) throws ExceptionConnect {
        try {
            String query = "update motos_conces set dni_concesionari=" + concesionari.getDni_concesionari() + " where motos_matricula=" + matricula;
            Statement mystmn = myconn.createStatement();
            ResultSet rs = mystmn.executeQuery(query);

            if (concesionari.getMagatzem_motos().size() < concesionari.getCantmaxmotos()) {
                while (rs.next()) {
                    System.out.println(rs.getString(1) + " " + rs.getString(2));
                }
                System.out.println("moto amb matricula :" + matricula + ", canviada de concesionari  ");
            } else {
                System.out.println("En el concesionari ja hi ha la cantitat maxima de motos");
            }

        } catch (Exception e) {
//            logger.error("No s'ha conectat correctament ", e);
//            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }

    @Override
    public void closeConnection()  {
        ConexioJDBC conexioJDBC = new ConexioJDBC();
        myconn = conexioJDBC.close();


    }
}
